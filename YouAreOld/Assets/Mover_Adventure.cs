﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Mover_Adventure : MonoBehaviour
{
    [SerializeField]
    float topSpeed;

    [SerializeField]
    float moveForce;

    [SerializeField]
    float drag;

    Rigidbody rigid;

    void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        rigid.drag = drag;
    }
    
    void Update()
    {
        LimitSpeed();
    }

    public void Move(Vector2 vector)
    {
        Vector3 v = new Vector3(vector.x, 0, vector.y);
        rigid.AddForce(v.normalized * moveForce * Time.deltaTime);        
    }

    void LimitSpeed()
    {
        if (rigid.velocity.magnitude > topSpeed)
        {
            rigid.AddForce(-rigid.velocity.normalized * moveForce * Time.deltaTime);
        }
    }
}