﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerCharacterController : MonoBehaviour
{
    public float hAxis;
    public float vAxis;

    public UnityEvent OnFire = new UnityEvent();

    void Update()
    {
        hAxis = Input.GetAxis("Horizontal");
        vAxis = Input.GetAxis("Vertical");

        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnFire.Invoke();
        }
    }
}
