﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PC_Deli : MonoBehaviour
{
    PlayerCharacterController controller;
    Mover_Adventure mover;

    void Awake()
    {
        controller = GetComponent<PlayerCharacterController>();
        mover = GetComponent<Mover_Adventure>();
    }

    void Update()
    {
        Vector2 movement = new Vector2(controller.hAxis, controller.vAxis);

        mover.Move(movement);
    }
}
