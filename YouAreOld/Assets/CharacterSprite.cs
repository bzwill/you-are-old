﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CharacterSprite : MonoBehaviour
{
    [SerializeField]
    Texture characterSprite;

    void Awake()
    {
        GetComponent<Renderer>().material.SetTexture("_MainTex", characterSprite);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
